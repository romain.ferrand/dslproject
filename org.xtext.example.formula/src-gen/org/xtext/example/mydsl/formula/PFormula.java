/**
 * generated by Xtext 2.15.0
 */
package org.xtext.example.mydsl.formula;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PFormula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.formula.PFormula#getExp <em>Exp</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.formula.FormulaPackage#getPFormula()
 * @model
 * @generated
 */
public interface PFormula extends Formula
{
  /**
   * Returns the value of the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exp</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exp</em>' containment reference.
   * @see #setExp(Formula)
   * @see org.xtext.example.mydsl.formula.FormulaPackage#getPFormula_Exp()
   * @model containment="true"
   * @generated
   */
  Formula getExp();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.formula.PFormula#getExp <em>Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exp</em>' containment reference.
   * @see #getExp()
   * @generated
   */
  void setExp(Formula value);

} // PFormula
