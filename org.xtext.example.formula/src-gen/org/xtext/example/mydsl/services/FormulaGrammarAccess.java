/*
 * generated by Xtext 2.15.0
 */
package org.xtext.example.mydsl.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder.AbstractGrammarElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class FormulaGrammarAccess extends AbstractGrammarElementFinder {
	
	public class SolveElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Solve");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Alternatives cAlternatives_0 = (Alternatives)cGroup.eContents().get(0);
		private final RuleCall cAllSolverParserRuleCall_0_0 = (RuleCall)cAlternatives_0.eContents().get(0);
		private final RuleCall cTestParserRuleCall_0_1 = (RuleCall)cAlternatives_0.eContents().get(1);
		private final Action cSolveTestsAction_1 = (Action)cGroup.eContents().get(1);
		private final Assignment cTestsAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final Alternatives cTestsAlternatives_2_0 = (Alternatives)cTestsAssignment_2.eContents().get(0);
		private final RuleCall cTestsTestParserRuleCall_2_0_0 = (RuleCall)cTestsAlternatives_2_0.eContents().get(0);
		private final RuleCall cTestsAllSolverParserRuleCall_2_0_1 = (RuleCall)cTestsAlternatives_2_0.eContents().get(1);
		
		//Solve:
		//	(AllSolver | Test) {Solve.tests+=current} tests+=(Test | AllSolver)*;
		@Override public ParserRule getRule() { return rule; }
		
		//(AllSolver | Test) {Solve.tests+=current} tests+=(Test | AllSolver)*
		public Group getGroup() { return cGroup; }
		
		//AllSolver | Test
		public Alternatives getAlternatives_0() { return cAlternatives_0; }
		
		//AllSolver
		public RuleCall getAllSolverParserRuleCall_0_0() { return cAllSolverParserRuleCall_0_0; }
		
		//Test
		public RuleCall getTestParserRuleCall_0_1() { return cTestParserRuleCall_0_1; }
		
		//{Solve.tests+=current}
		public Action getSolveTestsAction_1() { return cSolveTestsAction_1; }
		
		//tests+=(Test | AllSolver)*
		public Assignment getTestsAssignment_2() { return cTestsAssignment_2; }
		
		//(Test | AllSolver)
		public Alternatives getTestsAlternatives_2_0() { return cTestsAlternatives_2_0; }
		
		//Test
		public RuleCall getTestsTestParserRuleCall_2_0_0() { return cTestsTestParserRuleCall_2_0_0; }
		
		//AllSolver
		public RuleCall getTestsAllSolverParserRuleCall_2_0_1() { return cTestsAllSolverParserRuleCall_2_0_1; }
	}
	public class TestElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Test");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cTestKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cFormulaAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cFormulaEquivParserRuleCall_1_0 = (RuleCall)cFormulaAssignment_1.eContents().get(0);
		private final Keyword cWithKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cSolverAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cSolverSatsolverParserRuleCall_3_0 = (RuleCall)cSolverAssignment_3.eContents().get(0);
		private final Keyword cSemicolonKeyword_4 = (Keyword)cGroup.eContents().get(4);
		
		//Test:
		//	"Test" formula=Equiv "with" solver=Satsolver ";";
		@Override public ParserRule getRule() { return rule; }
		
		//"Test" formula=Equiv "with" solver=Satsolver ";"
		public Group getGroup() { return cGroup; }
		
		//"Test"
		public Keyword getTestKeyword_0() { return cTestKeyword_0; }
		
		//formula=Equiv
		public Assignment getFormulaAssignment_1() { return cFormulaAssignment_1; }
		
		//Equiv
		public RuleCall getFormulaEquivParserRuleCall_1_0() { return cFormulaEquivParserRuleCall_1_0; }
		
		//"with"
		public Keyword getWithKeyword_2() { return cWithKeyword_2; }
		
		//solver=Satsolver
		public Assignment getSolverAssignment_3() { return cSolverAssignment_3; }
		
		//Satsolver
		public RuleCall getSolverSatsolverParserRuleCall_3_0() { return cSolverSatsolverParserRuleCall_3_0; }
		
		//";"
		public Keyword getSemicolonKeyword_4() { return cSemicolonKeyword_4; }
	}
	public class AllSolverElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.AllSolver");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cAllSolverKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cFormulaAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cFormulaEquivParserRuleCall_1_0 = (RuleCall)cFormulaAssignment_1.eContents().get(0);
		private final Keyword cSemicolonKeyword_2 = (Keyword)cGroup.eContents().get(2);
		
		//AllSolver:
		//	"AllSolver" formula=Equiv ";";
		@Override public ParserRule getRule() { return rule; }
		
		//"AllSolver" formula=Equiv ";"
		public Group getGroup() { return cGroup; }
		
		//"AllSolver"
		public Keyword getAllSolverKeyword_0() { return cAllSolverKeyword_0; }
		
		//formula=Equiv
		public Assignment getFormulaAssignment_1() { return cFormulaAssignment_1; }
		
		//Equiv
		public RuleCall getFormulaEquivParserRuleCall_1_0() { return cFormulaEquivParserRuleCall_1_0; }
		
		//";"
		public Keyword getSemicolonKeyword_2() { return cSemicolonKeyword_2; }
	}
	public class SatsolverElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Satsolver");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final Assignment cNameAssignment_0 = (Assignment)cAlternatives.eContents().get(0);
		private final Keyword cNameSAT4JKeyword_0_0 = (Keyword)cNameAssignment_0.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cAlternatives.eContents().get(1);
		private final Keyword cNameMinisatKeyword_1_0 = (Keyword)cNameAssignment_1.eContents().get(0);
		
		//Satsolver:
		//	name="SAT4J" | name="minisat";
		@Override public ParserRule getRule() { return rule; }
		
		//name="SAT4J" | name="minisat"
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//name="SAT4J"
		public Assignment getNameAssignment_0() { return cNameAssignment_0; }
		
		//"SAT4J"
		public Keyword getNameSAT4JKeyword_0_0() { return cNameSAT4JKeyword_0_0; }
		
		//name="minisat"
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }
		
		//"minisat"
		public Keyword getNameMinisatKeyword_1_0() { return cNameMinisatKeyword_1_0; }
	}
	public class EquivElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Equiv");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cImplieParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cEquivLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Keyword cLessThanSignEqualsSignGreaterThanSignKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightImplieParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//Equiv Formula:
		//	Implie ({Equiv.left=current} "<=>" right=Implie)*;
		@Override public ParserRule getRule() { return rule; }
		
		//Implie ({Equiv.left=current} "<=>" right=Implie)*
		public Group getGroup() { return cGroup; }
		
		//Implie
		public RuleCall getImplieParserRuleCall_0() { return cImplieParserRuleCall_0; }
		
		//({Equiv.left=current} "<=>" right=Implie)*
		public Group getGroup_1() { return cGroup_1; }
		
		//{Equiv.left=current}
		public Action getEquivLeftAction_1_0() { return cEquivLeftAction_1_0; }
		
		//"<=>"
		public Keyword getLessThanSignEqualsSignGreaterThanSignKeyword_1_1() { return cLessThanSignEqualsSignGreaterThanSignKeyword_1_1; }
		
		//right=Implie
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }
		
		//Implie
		public RuleCall getRightImplieParserRuleCall_1_2_0() { return cRightImplieParserRuleCall_1_2_0; }
	}
	public class ImplieElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Implie");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cOrParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cImplieLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Keyword cEqualsSignGreaterThanSignKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightOrParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//Implie Formula:
		//	Or ({Implie.left=current} "=>" right=Or)*;
		@Override public ParserRule getRule() { return rule; }
		
		//Or ({Implie.left=current} "=>" right=Or)*
		public Group getGroup() { return cGroup; }
		
		//Or
		public RuleCall getOrParserRuleCall_0() { return cOrParserRuleCall_0; }
		
		//({Implie.left=current} "=>" right=Or)*
		public Group getGroup_1() { return cGroup_1; }
		
		//{Implie.left=current}
		public Action getImplieLeftAction_1_0() { return cImplieLeftAction_1_0; }
		
		//"=>"
		public Keyword getEqualsSignGreaterThanSignKeyword_1_1() { return cEqualsSignGreaterThanSignKeyword_1_1; }
		
		//right=Or
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }
		
		//Or
		public RuleCall getRightOrParserRuleCall_1_2_0() { return cRightOrParserRuleCall_1_2_0; }
	}
	public class OrElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Or");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cAndParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cOrLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Keyword cOrKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightNorParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//Or Formula:
		//	And ({Or.left=current} "or" right=Nor)*;
		@Override public ParserRule getRule() { return rule; }
		
		//And ({Or.left=current} "or" right=Nor)*
		public Group getGroup() { return cGroup; }
		
		//And
		public RuleCall getAndParserRuleCall_0() { return cAndParserRuleCall_0; }
		
		//({Or.left=current} "or" right=Nor)*
		public Group getGroup_1() { return cGroup_1; }
		
		//{Or.left=current}
		public Action getOrLeftAction_1_0() { return cOrLeftAction_1_0; }
		
		//"or"
		public Keyword getOrKeyword_1_1() { return cOrKeyword_1_1; }
		
		//right=Nor
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }
		
		//Nor
		public RuleCall getRightNorParserRuleCall_1_2_0() { return cRightNorParserRuleCall_1_2_0; }
	}
	public class NorElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Nor");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cAndParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cNorLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Keyword cNorKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightAndParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//Nor Formula:
		//	And ({Nor.left=current} "nor" right=And)*;
		@Override public ParserRule getRule() { return rule; }
		
		//And ({Nor.left=current} "nor" right=And)*
		public Group getGroup() { return cGroup; }
		
		//And
		public RuleCall getAndParserRuleCall_0() { return cAndParserRuleCall_0; }
		
		//({Nor.left=current} "nor" right=And)*
		public Group getGroup_1() { return cGroup_1; }
		
		//{Nor.left=current}
		public Action getNorLeftAction_1_0() { return cNorLeftAction_1_0; }
		
		//"nor"
		public Keyword getNorKeyword_1_1() { return cNorKeyword_1_1; }
		
		//right=And
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }
		
		//And
		public RuleCall getRightAndParserRuleCall_1_2_0() { return cRightAndParserRuleCall_1_2_0; }
	}
	public class AndElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.And");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cNandParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cAndLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Keyword cAndKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightNandParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//And Formula:
		//	Nand ({And.left=current} "and" right=Nand)*;
		@Override public ParserRule getRule() { return rule; }
		
		//Nand ({And.left=current} "and" right=Nand)*
		public Group getGroup() { return cGroup; }
		
		//Nand
		public RuleCall getNandParserRuleCall_0() { return cNandParserRuleCall_0; }
		
		//({And.left=current} "and" right=Nand)*
		public Group getGroup_1() { return cGroup_1; }
		
		//{And.left=current}
		public Action getAndLeftAction_1_0() { return cAndLeftAction_1_0; }
		
		//"and"
		public Keyword getAndKeyword_1_1() { return cAndKeyword_1_1; }
		
		//right=Nand
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }
		
		//Nand
		public RuleCall getRightNandParserRuleCall_1_2_0() { return cRightNandParserRuleCall_1_2_0; }
	}
	public class NandElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Nand");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Alternatives cAlternatives_0 = (Alternatives)cGroup.eContents().get(0);
		private final RuleCall cNotParserRuleCall_0_0 = (RuleCall)cAlternatives_0.eContents().get(0);
		private final RuleCall cPrimaryParserRuleCall_0_1 = (RuleCall)cAlternatives_0.eContents().get(1);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cNandLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Keyword cNandKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final Alternatives cRightAlternatives_1_2_0 = (Alternatives)cRightAssignment_1_2.eContents().get(0);
		private final RuleCall cRightNotParserRuleCall_1_2_0_0 = (RuleCall)cRightAlternatives_1_2_0.eContents().get(0);
		private final RuleCall cRightPrimaryParserRuleCall_1_2_0_1 = (RuleCall)cRightAlternatives_1_2_0.eContents().get(1);
		
		//Nand Formula:
		//	(Not | Primary) ({Nand.left=current} "nand" right=(Not | Primary))*;
		@Override public ParserRule getRule() { return rule; }
		
		//(Not | Primary) ({Nand.left=current} "nand" right=(Not | Primary))*
		public Group getGroup() { return cGroup; }
		
		//Not | Primary
		public Alternatives getAlternatives_0() { return cAlternatives_0; }
		
		//Not
		public RuleCall getNotParserRuleCall_0_0() { return cNotParserRuleCall_0_0; }
		
		//Primary
		public RuleCall getPrimaryParserRuleCall_0_1() { return cPrimaryParserRuleCall_0_1; }
		
		//({Nand.left=current} "nand" right=(Not | Primary))*
		public Group getGroup_1() { return cGroup_1; }
		
		//{Nand.left=current}
		public Action getNandLeftAction_1_0() { return cNandLeftAction_1_0; }
		
		//"nand"
		public Keyword getNandKeyword_1_1() { return cNandKeyword_1_1; }
		
		//right=(Not | Primary)
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }
		
		//(Not | Primary)
		public Alternatives getRightAlternatives_1_2_0() { return cRightAlternatives_1_2_0; }
		
		//Not
		public RuleCall getRightNotParserRuleCall_1_2_0_0() { return cRightNotParserRuleCall_1_2_0_0; }
		
		//Primary
		public RuleCall getRightPrimaryParserRuleCall_1_2_0_1() { return cRightPrimaryParserRuleCall_1_2_0_1; }
	}
	public class NotElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Not");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cExclamationMarkKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final RuleCall cPrimaryParserRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final Action cNotExpAction_2 = (Action)cGroup.eContents().get(2);
		
		//Not Formula:
		//	"!" Primary {Not.exp=current};
		@Override public ParserRule getRule() { return rule; }
		
		//"!" Primary {Not.exp=current}
		public Group getGroup() { return cGroup; }
		
		//"!"
		public Keyword getExclamationMarkKeyword_0() { return cExclamationMarkKeyword_0; }
		
		//Primary
		public RuleCall getPrimaryParserRuleCall_1() { return cPrimaryParserRuleCall_1; }
		
		//{Not.exp=current}
		public Action getNotExpAction_2() { return cNotExpAction_2; }
	}
	public class PrimaryElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Primary");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cAtomicParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cPFormulaParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//Primary Formula:
		//	Atomic | PFormula;
		@Override public ParserRule getRule() { return rule; }
		
		//Atomic | PFormula
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//Atomic
		public RuleCall getAtomicParserRuleCall_0() { return cAtomicParserRuleCall_0; }
		
		//PFormula
		public RuleCall getPFormulaParserRuleCall_1() { return cPFormulaParserRuleCall_1; }
	}
	public class AtomicElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.Atomic");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final Group cGroup_0 = (Group)cAlternatives.eContents().get(0);
		private final Action cVarPropAction_0_0 = (Action)cGroup_0.eContents().get(0);
		private final Assignment cIdAssignment_0_1 = (Assignment)cGroup_0.eContents().get(1);
		private final RuleCall cIdIDTerminalRuleCall_0_1_0 = (RuleCall)cIdAssignment_0_1.eContents().get(0);
		private final Group cGroup_1 = (Group)cAlternatives.eContents().get(1);
		private final Action cBoolConstantAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cValueAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Alternatives cValueAlternatives_1_1_0 = (Alternatives)cValueAssignment_1_1.eContents().get(0);
		private final Keyword cValueTrueKeyword_1_1_0_0 = (Keyword)cValueAlternatives_1_1_0.eContents().get(0);
		private final Keyword cValueFalseKeyword_1_1_0_1 = (Keyword)cValueAlternatives_1_1_0.eContents().get(1);
		
		//Atomic Formula:
		//	{VarProp} id=ID | {BoolConstant} value=('true' | 'false');
		@Override public ParserRule getRule() { return rule; }
		
		//{VarProp} id=ID | {BoolConstant} value=('true' | 'false')
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//{VarProp} id=ID
		public Group getGroup_0() { return cGroup_0; }
		
		//{VarProp}
		public Action getVarPropAction_0_0() { return cVarPropAction_0_0; }
		
		//id=ID
		public Assignment getIdAssignment_0_1() { return cIdAssignment_0_1; }
		
		//ID
		public RuleCall getIdIDTerminalRuleCall_0_1_0() { return cIdIDTerminalRuleCall_0_1_0; }
		
		//{BoolConstant} value=('true' | 'false')
		public Group getGroup_1() { return cGroup_1; }
		
		//{BoolConstant}
		public Action getBoolConstantAction_1_0() { return cBoolConstantAction_1_0; }
		
		//value=('true' | 'false')
		public Assignment getValueAssignment_1_1() { return cValueAssignment_1_1; }
		
		//('true' | 'false')
		public Alternatives getValueAlternatives_1_1_0() { return cValueAlternatives_1_1_0; }
		
		//'true'
		public Keyword getValueTrueKeyword_1_1_0_0() { return cValueTrueKeyword_1_1_0_0; }
		
		//'false'
		public Keyword getValueFalseKeyword_1_1_0_1() { return cValueFalseKeyword_1_1_0_1; }
	}
	public class PFormulaElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.xtext.example.mydsl.Formula.PFormula");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cLeftParenthesisKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final RuleCall cEquivParserRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final Action cPFormulaExpAction_2 = (Action)cGroup.eContents().get(2);
		private final Keyword cRightParenthesisKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//PFormula Formula:
		//	'(' Equiv {PFormula.exp=current} ')';
		@Override public ParserRule getRule() { return rule; }
		
		//'(' Equiv {PFormula.exp=current} ')'
		public Group getGroup() { return cGroup; }
		
		//'('
		public Keyword getLeftParenthesisKeyword_0() { return cLeftParenthesisKeyword_0; }
		
		//Equiv
		public RuleCall getEquivParserRuleCall_1() { return cEquivParserRuleCall_1; }
		
		//{PFormula.exp=current}
		public Action getPFormulaExpAction_2() { return cPFormulaExpAction_2; }
		
		//')'
		public Keyword getRightParenthesisKeyword_3() { return cRightParenthesisKeyword_3; }
	}
	
	
	private final SolveElements pSolve;
	private final TestElements pTest;
	private final AllSolverElements pAllSolver;
	private final SatsolverElements pSatsolver;
	private final EquivElements pEquiv;
	private final ImplieElements pImplie;
	private final OrElements pOr;
	private final NorElements pNor;
	private final AndElements pAnd;
	private final NandElements pNand;
	private final NotElements pNot;
	private final PrimaryElements pPrimary;
	private final AtomicElements pAtomic;
	private final PFormulaElements pPFormula;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public FormulaGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pSolve = new SolveElements();
		this.pTest = new TestElements();
		this.pAllSolver = new AllSolverElements();
		this.pSatsolver = new SatsolverElements();
		this.pEquiv = new EquivElements();
		this.pImplie = new ImplieElements();
		this.pOr = new OrElements();
		this.pNor = new NorElements();
		this.pAnd = new AndElements();
		this.pNand = new NandElements();
		this.pNot = new NotElements();
		this.pPrimary = new PrimaryElements();
		this.pAtomic = new AtomicElements();
		this.pPFormula = new PFormulaElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("org.xtext.example.mydsl.Formula".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//Solve:
	//	(AllSolver | Test) {Solve.tests+=current} tests+=(Test | AllSolver)*;
	public SolveElements getSolveAccess() {
		return pSolve;
	}
	
	public ParserRule getSolveRule() {
		return getSolveAccess().getRule();
	}
	
	//Test:
	//	"Test" formula=Equiv "with" solver=Satsolver ";";
	public TestElements getTestAccess() {
		return pTest;
	}
	
	public ParserRule getTestRule() {
		return getTestAccess().getRule();
	}
	
	//AllSolver:
	//	"AllSolver" formula=Equiv ";";
	public AllSolverElements getAllSolverAccess() {
		return pAllSolver;
	}
	
	public ParserRule getAllSolverRule() {
		return getAllSolverAccess().getRule();
	}
	
	//Satsolver:
	//	name="SAT4J" | name="minisat";
	public SatsolverElements getSatsolverAccess() {
		return pSatsolver;
	}
	
	public ParserRule getSatsolverRule() {
		return getSatsolverAccess().getRule();
	}
	
	//Equiv Formula:
	//	Implie ({Equiv.left=current} "<=>" right=Implie)*;
	public EquivElements getEquivAccess() {
		return pEquiv;
	}
	
	public ParserRule getEquivRule() {
		return getEquivAccess().getRule();
	}
	
	//Implie Formula:
	//	Or ({Implie.left=current} "=>" right=Or)*;
	public ImplieElements getImplieAccess() {
		return pImplie;
	}
	
	public ParserRule getImplieRule() {
		return getImplieAccess().getRule();
	}
	
	//Or Formula:
	//	And ({Or.left=current} "or" right=Nor)*;
	public OrElements getOrAccess() {
		return pOr;
	}
	
	public ParserRule getOrRule() {
		return getOrAccess().getRule();
	}
	
	//Nor Formula:
	//	And ({Nor.left=current} "nor" right=And)*;
	public NorElements getNorAccess() {
		return pNor;
	}
	
	public ParserRule getNorRule() {
		return getNorAccess().getRule();
	}
	
	//And Formula:
	//	Nand ({And.left=current} "and" right=Nand)*;
	public AndElements getAndAccess() {
		return pAnd;
	}
	
	public ParserRule getAndRule() {
		return getAndAccess().getRule();
	}
	
	//Nand Formula:
	//	(Not | Primary) ({Nand.left=current} "nand" right=(Not | Primary))*;
	public NandElements getNandAccess() {
		return pNand;
	}
	
	public ParserRule getNandRule() {
		return getNandAccess().getRule();
	}
	
	//Not Formula:
	//	"!" Primary {Not.exp=current};
	public NotElements getNotAccess() {
		return pNot;
	}
	
	public ParserRule getNotRule() {
		return getNotAccess().getRule();
	}
	
	//Primary Formula:
	//	Atomic | PFormula;
	public PrimaryElements getPrimaryAccess() {
		return pPrimary;
	}
	
	public ParserRule getPrimaryRule() {
		return getPrimaryAccess().getRule();
	}
	
	//Atomic Formula:
	//	{VarProp} id=ID | {BoolConstant} value=('true' | 'false');
	public AtomicElements getAtomicAccess() {
		return pAtomic;
	}
	
	public ParserRule getAtomicRule() {
		return getAtomicAccess().getRule();
	}
	
	//PFormula Formula:
	//	'(' Equiv {PFormula.exp=current} ')';
	public PFormulaElements getPFormulaAccess() {
		return pPFormula;
	}
	
	public ParserRule getPFormulaRule() {
		return getPFormulaAccess().getRule();
	}
	
	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' |
	//	"'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
