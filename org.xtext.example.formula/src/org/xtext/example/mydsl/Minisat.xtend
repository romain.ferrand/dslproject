package org.xtext.example.mydsl
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList

class Minisat extends Solver {
	
	override test(String file) {
		var commands = new ArrayList<String>();
	    commands.add("/bin/sh");
	    commands.add("-c");
	    commands.add("echo \"" + file + "\" | /usr/bin/minisat | tail -1");
	    println(file)
		val p = Runtime.getRuntime().exec(commands)
		p.waitFor()
		val reader = new BufferedReader(new InputStreamReader(p.getInputStream()))
		val res = reader.readLine()
		"SATISFIABLE" == res
	}
	
}