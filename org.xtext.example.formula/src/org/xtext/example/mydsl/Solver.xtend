package org.xtext.example.mydsl

import java.util.Vector
import org.eclipse.emf.ecore.util.EcoreUtil.Copier
import org.eclipse.emf.ecore.EObject
import java.util.LinkedHashMap
import org.xtext.example.mydsl.formula.Formula
import org.xtext.example.mydsl.formula.And
import org.xtext.example.mydsl.formula.Nand
import org.xtext.example.mydsl.formula.Nor
import org.xtext.example.mydsl.formula.Equiv
import org.xtext.example.mydsl.formula.Implie
import org.xtext.example.mydsl.formula.Not
import org.xtext.example.mydsl.formula.Or
import org.xtext.example.mydsl.formula.VarProp
import org.xtext.example.mydsl.formula.PFormula
import org.xtext.example.mydsl.formula.FormulaFactory
import java.util.Random
import java.nio.charset.Charset
import org.xtext.example.mydsl.formula.Test

class Pair {
	public final Formula p
	public final Formula c
	new(Formula x, Formula y){
		this.p = x
		this.c = y
	}
}
abstract class Solver {
		val copier = new Copier()
	
	def <T extends EObject> copy(T obj) {
		copier.copy(obj) as T
	}
	def getRandomString() {
		val byte[] array = newByteArrayOfSize(7)
		new Random().nextBytes(array)
		val str = new String(array, Charset.forName("UTF-8"))
		str
	}
	def Pair tseitin (Formula fm){
		switch fm {
			PFormula:
				tseitin(fm.exp.copy)
			Equiv: {
				val f1 = tseitin(fm.left.copy)
				val f2 = tseitin(fm.right.copy)
				val p = FormulaFactory.eINSTANCE.createVarProp()
				val and_1 = FormulaFactory.eINSTANCE.createAnd()
				val and_2 = FormulaFactory.eINSTANCE.createAnd()
				val and_3 = FormulaFactory.eINSTANCE.createAnd()
				val and_4 = FormulaFactory.eINSTANCE.createAnd()
				val and_5 = FormulaFactory.eINSTANCE.createAnd()
				val or_1 = FormulaFactory.eINSTANCE.createOr()
				val or_2 = FormulaFactory.eINSTANCE.createOr()
				val or_3 = FormulaFactory.eINSTANCE.createOr()
				val or_4 = FormulaFactory.eINSTANCE.createOr()
				val or_5 = FormulaFactory.eINSTANCE.createOr()
				val or_6 = FormulaFactory.eINSTANCE.createOr()
				val or_7 = FormulaFactory.eINSTANCE.createOr()
				val or_8 = FormulaFactory.eINSTANCE.createOr()
				val not_p = FormulaFactory.eINSTANCE.createNot()
				val not_p1 = FormulaFactory.eINSTANCE.createNot()
				val not_p2 = FormulaFactory.eINSTANCE.createNot()
				val p1 = f1.p.copy
				val p2 = f2.p.copy
				p.id = getRandomString()
				not_p.exp = p.copy
				not_p1.exp = p1.copy
				not_p2.exp = p2.copy
				or_1.left = not_p.copy
				or_2.left = not_p1.copy
				or_2.right = p2.copy
				or_1.right = or_2
				or_3.left = not_p.copy
				or_4.left = p1.copy
				or_4.right = not_p2.copy
				or_3.right = or_4
				or_5.left = p.copy
				or_6.left = p1.copy
				or_6.right = p2.copy
				or_5.right = or_6
				or_7.left = p.copy
				or_8.left = not_p1.copy
				or_8.right = not_p2.copy
				or_7.right = or_8
				and_1.left = f1.c.copy
				and_1.right = f2.c.copy
				and_2.left = or_7
				and_2.right = and_1
				and_3.left = or_5
				and_3.right = and_2
				and_4.left = or_3
				and_4.right = and_3
				and_5.left = or_1
				and_5.right = and_4
				return new Pair(p.copy, and_5)
				}
			Implie:{
				val f1 = tseitin(fm.left.copy)
				val f2 = tseitin(fm.right.copy)
				val p = FormulaFactory.eINSTANCE.createVarProp()
				val or_1 = FormulaFactory.eINSTANCE.createOr()
				val or_2 = FormulaFactory.eINSTANCE.createOr()
				val or_3 = FormulaFactory.eINSTANCE.createOr()
				val or_4 = FormulaFactory.eINSTANCE.createOr()
				val and_1 = FormulaFactory.eINSTANCE.createAnd()
				val and_2 = FormulaFactory.eINSTANCE.createAnd()
				val and_3 = FormulaFactory.eINSTANCE.createAnd()
				val and_4 = FormulaFactory.eINSTANCE.createAnd()
				val not_p = FormulaFactory.eINSTANCE.createNot()
				val not_p1 = FormulaFactory.eINSTANCE.createNot()
				val not_p2 = FormulaFactory.eINSTANCE.createNot()
				p.id = getRandomString()
				not_p.exp = p.copy
				not_p1.exp = f1.p.copy
				not_p2.exp = f2.p.copy
				or_1.left = not_p.copy
				or_2.left = not_p1.copy
				or_2.right = f2.p.copy
				or_1.right = or_2.copy
				or_3.left = p.copy
				or_3.right = f1.p.copy
				or_4.left = p.copy
				or_4.right = not_p2.copy
				and_1.left = f1.c.copy
				and_1.right = f2.c.copy
				and_2.left = or_4.copy
				and_2.right = and_1.copy
				and_3.left = or_3.copy
				and_3.right = and_2.copy
				and_4.left = or_1.copy
				and_4.right = and_3.copy
				return new Pair(p.copy, and_4.copy)
				}
			And: {
				val f1 = tseitin(fm.left.copy)
				val f2 = tseitin(fm.right.copy)
				val p = FormulaFactory.eINSTANCE.createVarProp()
				val or_1 = FormulaFactory.eINSTANCE.createOr()
				val or_2 = FormulaFactory.eINSTANCE.createOr()
				val or_3 = FormulaFactory.eINSTANCE.createOr()
				val or_4 = FormulaFactory.eINSTANCE.createOr()
				val and_1 = FormulaFactory.eINSTANCE.createAnd()
				val and_2 = FormulaFactory.eINSTANCE.createAnd()
				val and_3 = FormulaFactory.eINSTANCE.createAnd()
				val and_4 = FormulaFactory.eINSTANCE.createAnd()
				val not_p = FormulaFactory.eINSTANCE.createNot()
				val not_p1 = FormulaFactory.eINSTANCE.createNot()
				val not_p2 = FormulaFactory.eINSTANCE.createNot()
				p.id = getRandomString()
				not_p.exp = p.copy
				not_p1.exp = f1.p.copy
				not_p2.exp = f2.p.copy
				or_1.left = p.copy
				or_2.left = not_p1.copy
				or_2.right = not_p2.copy
				or_1.right = or_2.copy
				or_3.left = not_p.copy
				or_3.right = f1.p.copy
				or_4.left = not_p.copy
				or_4.right = f2.p.copy
				and_1.left = f1.c.copy
				and_1.right = f2.c.copy
				and_2.left = or_4.copy
				and_2.right = and_1.copy
				and_3.left = or_3.copy
				and_3.right = and_2.copy
				and_4.left = or_1.copy
				and_4.right = and_3.copy
				return new Pair(p.copy, and_4.copy)
				}
		Nand: {
			val or = FormulaFactory.eINSTANCE.createOr()
			val not_left = FormulaFactory.eINSTANCE.createNot()
			val not_right = FormulaFactory.eINSTANCE.createNot()
			not_left.exp = fm.left.copy
			not_right.exp = fm.right.copy
			or.left = not_left.copy
			or.right = not_right.copy
			return tseitin(or.copy)
		}		
		Or: {
				val f1 = tseitin(fm.left.copy)
				val f2 = tseitin(fm.right.copy)
				val p = FormulaFactory.eINSTANCE.createVarProp()
				val or_1 = FormulaFactory.eINSTANCE.createOr()
				val or_2 = FormulaFactory.eINSTANCE.createOr()
				val or_3 = FormulaFactory.eINSTANCE.createOr()
				val or_4 = FormulaFactory.eINSTANCE.createOr()
				val and_1 = FormulaFactory.eINSTANCE.createAnd()
				val and_2 = FormulaFactory.eINSTANCE.createAnd()
				val and_3 = FormulaFactory.eINSTANCE.createAnd()
				val and_4 = FormulaFactory.eINSTANCE.createAnd()
				val not_p = FormulaFactory.eINSTANCE.createNot()
				val not_p1 = FormulaFactory.eINSTANCE.createNot()
				val not_p2 = FormulaFactory.eINSTANCE.createNot()
				p.id = getRandomString()
				not_p.exp = p.copy
				not_p1.exp = f1.p.copy
				not_p2.exp = f2.p.copy
				or_1.left = not_p.copy
				or_2.left = f1.p.copy
				or_2.right = f2.p.copy
				or_1.right = or_2.copy
				or_3.left = p.copy
				or_3.right = not_p1.copy
				or_4.left = p.copy
				or_4.right = not_p2.copy
				and_1.left = f1.c.copy
				and_1.right = f2.c.copy
				and_2.left = or_4.copy
				and_2.right = and_1.copy
				and_3.left = or_3.copy
				and_3.right = and_2.copy
				and_4.left = or_1.copy
				and_4.right = and_3.copy
				return new Pair(p.copy, and_4.copy)
		}
		Nor: {
			val and = FormulaFactory.eINSTANCE.createAnd()
			val not_left = FormulaFactory.eINSTANCE.createNot()
			val not_right = FormulaFactory.eINSTANCE.createNot()
			not_left.exp = fm.left.copy
			not_right.exp = fm.right.copy
			and.left = not_left.copy
			and.right = not_right.copy
			return tseitin(and.copy)
		}
		Not: {
			val f1 = tseitin(fm.exp.copy)
			val not_p = FormulaFactory.eINSTANCE.createNot()
			not_p.exp = f1.p.copy
			return new Pair(not_p.copy, f1.c.copy)
		}
		VarProp:
			return new Pair(fm.copy, null)
		}
	}
	def toDimacs(Formula fm, LinkedHashMap<String, Integer> toInd, Vector<String> clauses){
		switch fm {
			Equiv,
			Implie,
			Nand,
			Nor: throw new RuntimeException("Not in CNF")
			Or:
				return (toDimacs(fm.left, toInd, clauses)+" "+toDimacs(fm.right, toInd, clauses))
			And:{
				if(fm.left !== null)
					if(fm.left instanceof And)
						toDimacs(fm.left, toInd, clauses)
					else
						clauses.add(toDimacs(fm.left, toInd, clauses)+" 0\n")
				if(fm.right !== null)
					if(fm.right instanceof And)
						toDimacs(fm.right, toInd, clauses)
					else
						clauses.add(toDimacs(fm.right, toInd, clauses)+" 0\n")
				return ""
				}
			Not: {
				if(fm.exp instanceof Not) {
					val _not = fm.exp.copy as Not
					toDimacs(_not.exp.copy, toInd, clauses)
				} else {
					"-"+toDimacs(fm.exp, toInd, clauses)
				}
			}
			VarProp:{
				toInd?.get(fm.id)?:toInd.put(fm.id, toInd.size() + 1)
				return toInd.get(fm.id)
			}
		}
	}
	def String printAST(Formula fm){
		switch fm {
			PFormula:
				"("+printAST(fm.exp)+")"
			Equiv:
				"Equiv("+printAST(fm.left)+" , "+printAST(fm.right)+")"
			Implie:
				"Implie ("+printAST(fm.left)+" , "+printAST(fm.right)+")"
			Nor:
				"Nor("+printAST(fm.left)+" , "+printAST(fm.right)+")"
			Or:
				"Or ("+printAST(fm.left)+" , "+printAST(fm.right)+")"
			Nand:
				"Nand ("+printAST(fm.left)+" , "+printAST(fm.right)+")"
			And:
				"And ("+printAST(fm.left)+" , "+printAST(fm.right)+")"
			Not:
				"Not ("+printAST(fm.exp)+")"
			VarProp:
				fm.id
		}
	}
	def Boolean test(String file)
	def testSatisfiability(Formula formula){
		val f = tseitin(formula.copy)
		val and = FormulaFactory.eINSTANCE.createAnd()
		and.left = f.c.copy
		and.right = f.p.copy
		val toInd = newLinkedHashMap()
		val clauses = new Vector<String>
		toDimacs(and.copy, toInd, clauses)
		var res = ""
		for(str: clauses){
			res += str
		}
		val dimacsStr = String.format("p cnf %d %d\n", 
			toInd.size, clauses.size) + res
		if(test(dimacsStr))
			"satifiable"
		else
			"unsatifiable"
	}
}
