package org.xtext.example.mydsl;

import com.google.common.base.Objects;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.xtext.example.mydsl.Solver;

@SuppressWarnings("all")
public class Minisat extends Solver {
  @Override
  public Boolean test(final String file) {
    try {
      boolean _xblockexpression = false;
      {
        ArrayList<String> commands = new ArrayList<String>();
        commands.add("/bin/sh");
        commands.add("-c");
        commands.add((("echo \"" + file) + "\" | /usr/bin/minisat | tail -1"));
        InputOutput.<String>println(file);
        final ArrayList<String> _converted_commands = (ArrayList<String>)commands;
        final Process p = Runtime.getRuntime().exec(((String[])Conversions.unwrapArray(_converted_commands, String.class)));
        p.waitFor();
        InputStream _inputStream = p.getInputStream();
        InputStreamReader _inputStreamReader = new InputStreamReader(_inputStream);
        final BufferedReader reader = new BufferedReader(_inputStreamReader);
        final String res = reader.readLine();
        _xblockexpression = Objects.equal("SATISFIABLE", res);
      }
      return Boolean.valueOf(_xblockexpression);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
