package org.xtext.example.mydsl;

import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Vector;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.xtext.example.mydsl.Pair;
import org.xtext.example.mydsl.formula.And;
import org.xtext.example.mydsl.formula.Equiv;
import org.xtext.example.mydsl.formula.Formula;
import org.xtext.example.mydsl.formula.FormulaFactory;
import org.xtext.example.mydsl.formula.Implie;
import org.xtext.example.mydsl.formula.Nand;
import org.xtext.example.mydsl.formula.Nor;
import org.xtext.example.mydsl.formula.Not;
import org.xtext.example.mydsl.formula.Or;
import org.xtext.example.mydsl.formula.PFormula;
import org.xtext.example.mydsl.formula.VarProp;

@SuppressWarnings("all")
public abstract class Solver {
  private final EcoreUtil.Copier copier = new EcoreUtil.Copier();
  
  public <T extends EObject> T copy(final T obj) {
    EObject _copy = this.copier.copy(obj);
    return ((T) _copy);
  }
  
  public String getRandomString() {
    String _xblockexpression = null;
    {
      final byte[] array = new byte[7];
      new Random().nextBytes(array);
      Charset _forName = Charset.forName("UTF-8");
      final String str = new String(array, _forName);
      _xblockexpression = str;
    }
    return _xblockexpression;
  }
  
  public Pair tseitin(final Formula fm) {
    Pair _switchResult = null;
    boolean _matched = false;
    if (fm instanceof PFormula) {
      _matched=true;
      _switchResult = this.tseitin(this.<Formula>copy(((PFormula)fm).getExp()));
    }
    if (!_matched) {
      if (fm instanceof Equiv) {
        _matched=true;
        final Pair f1 = this.tseitin(this.<Formula>copy(((Equiv)fm).getLeft()));
        final Pair f2 = this.tseitin(this.<Formula>copy(((Equiv)fm).getRight()));
        final VarProp p = FormulaFactory.eINSTANCE.createVarProp();
        final And and_1 = FormulaFactory.eINSTANCE.createAnd();
        final And and_2 = FormulaFactory.eINSTANCE.createAnd();
        final And and_3 = FormulaFactory.eINSTANCE.createAnd();
        final And and_4 = FormulaFactory.eINSTANCE.createAnd();
        final And and_5 = FormulaFactory.eINSTANCE.createAnd();
        final Or or_1 = FormulaFactory.eINSTANCE.createOr();
        final Or or_2 = FormulaFactory.eINSTANCE.createOr();
        final Or or_3 = FormulaFactory.eINSTANCE.createOr();
        final Or or_4 = FormulaFactory.eINSTANCE.createOr();
        final Or or_5 = FormulaFactory.eINSTANCE.createOr();
        final Or or_6 = FormulaFactory.eINSTANCE.createOr();
        final Or or_7 = FormulaFactory.eINSTANCE.createOr();
        final Or or_8 = FormulaFactory.eINSTANCE.createOr();
        final Not not_p = FormulaFactory.eINSTANCE.createNot();
        final Not not_p1 = FormulaFactory.eINSTANCE.createNot();
        final Not not_p2 = FormulaFactory.eINSTANCE.createNot();
        final Formula p1 = this.<Formula>copy(f1.p);
        final Formula p2 = this.<Formula>copy(f2.p);
        p.setId(this.getRandomString());
        not_p.setExp(this.<VarProp>copy(p));
        not_p1.setExp(this.<Formula>copy(p1));
        not_p2.setExp(this.<Formula>copy(p2));
        or_1.setLeft(this.<Not>copy(not_p));
        or_2.setLeft(this.<Not>copy(not_p1));
        or_2.setRight(this.<Formula>copy(p2));
        or_1.setRight(or_2);
        or_3.setLeft(this.<Not>copy(not_p));
        or_4.setLeft(this.<Formula>copy(p1));
        or_4.setRight(this.<Not>copy(not_p2));
        or_3.setRight(or_4);
        or_5.setLeft(this.<VarProp>copy(p));
        or_6.setLeft(this.<Formula>copy(p1));
        or_6.setRight(this.<Formula>copy(p2));
        or_5.setRight(or_6);
        or_7.setLeft(this.<VarProp>copy(p));
        or_8.setLeft(this.<Not>copy(not_p1));
        or_8.setRight(this.<Not>copy(not_p2));
        or_7.setRight(or_8);
        and_1.setLeft(this.<Formula>copy(f1.c));
        and_1.setRight(this.<Formula>copy(f2.c));
        and_2.setLeft(or_7);
        and_2.setRight(and_1);
        and_3.setLeft(or_5);
        and_3.setRight(and_2);
        and_4.setLeft(or_3);
        and_4.setRight(and_3);
        and_5.setLeft(or_1);
        and_5.setRight(and_4);
        VarProp _copy = this.<VarProp>copy(p);
        return new Pair(_copy, and_5);
      }
    }
    if (!_matched) {
      if (fm instanceof Implie) {
        _matched=true;
        final Pair f1 = this.tseitin(this.<Formula>copy(((Implie)fm).getLeft()));
        final Pair f2 = this.tseitin(this.<Formula>copy(((Implie)fm).getRight()));
        final VarProp p = FormulaFactory.eINSTANCE.createVarProp();
        final Or or_1 = FormulaFactory.eINSTANCE.createOr();
        final Or or_2 = FormulaFactory.eINSTANCE.createOr();
        final Or or_3 = FormulaFactory.eINSTANCE.createOr();
        final Or or_4 = FormulaFactory.eINSTANCE.createOr();
        final And and_1 = FormulaFactory.eINSTANCE.createAnd();
        final And and_2 = FormulaFactory.eINSTANCE.createAnd();
        final And and_3 = FormulaFactory.eINSTANCE.createAnd();
        final And and_4 = FormulaFactory.eINSTANCE.createAnd();
        final Not not_p = FormulaFactory.eINSTANCE.createNot();
        final Not not_p1 = FormulaFactory.eINSTANCE.createNot();
        final Not not_p2 = FormulaFactory.eINSTANCE.createNot();
        p.setId(this.getRandomString());
        not_p.setExp(this.<VarProp>copy(p));
        not_p1.setExp(this.<Formula>copy(f1.p));
        not_p2.setExp(this.<Formula>copy(f2.p));
        or_1.setLeft(this.<Not>copy(not_p));
        or_2.setLeft(this.<Not>copy(not_p1));
        or_2.setRight(this.<Formula>copy(f2.p));
        or_1.setRight(this.<Or>copy(or_2));
        or_3.setLeft(this.<VarProp>copy(p));
        or_3.setRight(this.<Formula>copy(f1.p));
        or_4.setLeft(this.<VarProp>copy(p));
        or_4.setRight(this.<Not>copy(not_p2));
        and_1.setLeft(this.<Formula>copy(f1.c));
        and_1.setRight(this.<Formula>copy(f2.c));
        and_2.setLeft(this.<Or>copy(or_4));
        and_2.setRight(this.<And>copy(and_1));
        and_3.setLeft(this.<Or>copy(or_3));
        and_3.setRight(this.<And>copy(and_2));
        and_4.setLeft(this.<Or>copy(or_1));
        and_4.setRight(this.<And>copy(and_3));
        VarProp _copy = this.<VarProp>copy(p);
        And _copy_1 = this.<And>copy(and_4);
        return new Pair(_copy, _copy_1);
      }
    }
    if (!_matched) {
      if (fm instanceof And) {
        _matched=true;
        final Pair f1 = this.tseitin(this.<Formula>copy(((And)fm).getLeft()));
        final Pair f2 = this.tseitin(this.<Formula>copy(((And)fm).getRight()));
        final VarProp p = FormulaFactory.eINSTANCE.createVarProp();
        final Or or_1 = FormulaFactory.eINSTANCE.createOr();
        final Or or_2 = FormulaFactory.eINSTANCE.createOr();
        final Or or_3 = FormulaFactory.eINSTANCE.createOr();
        final Or or_4 = FormulaFactory.eINSTANCE.createOr();
        final And and_1 = FormulaFactory.eINSTANCE.createAnd();
        final And and_2 = FormulaFactory.eINSTANCE.createAnd();
        final And and_3 = FormulaFactory.eINSTANCE.createAnd();
        final And and_4 = FormulaFactory.eINSTANCE.createAnd();
        final Not not_p = FormulaFactory.eINSTANCE.createNot();
        final Not not_p1 = FormulaFactory.eINSTANCE.createNot();
        final Not not_p2 = FormulaFactory.eINSTANCE.createNot();
        p.setId(this.getRandomString());
        not_p.setExp(this.<VarProp>copy(p));
        not_p1.setExp(this.<Formula>copy(f1.p));
        not_p2.setExp(this.<Formula>copy(f2.p));
        or_1.setLeft(this.<VarProp>copy(p));
        or_2.setLeft(this.<Not>copy(not_p1));
        or_2.setRight(this.<Not>copy(not_p2));
        or_1.setRight(this.<Or>copy(or_2));
        or_3.setLeft(this.<Not>copy(not_p));
        or_3.setRight(this.<Formula>copy(f1.p));
        or_4.setLeft(this.<Not>copy(not_p));
        or_4.setRight(this.<Formula>copy(f2.p));
        and_1.setLeft(this.<Formula>copy(f1.c));
        and_1.setRight(this.<Formula>copy(f2.c));
        and_2.setLeft(this.<Or>copy(or_4));
        and_2.setRight(this.<And>copy(and_1));
        and_3.setLeft(this.<Or>copy(or_3));
        and_3.setRight(this.<And>copy(and_2));
        and_4.setLeft(this.<Or>copy(or_1));
        and_4.setRight(this.<And>copy(and_3));
        VarProp _copy = this.<VarProp>copy(p);
        And _copy_1 = this.<And>copy(and_4);
        return new Pair(_copy, _copy_1);
      }
    }
    if (!_matched) {
      if (fm instanceof Nand) {
        _matched=true;
        final Or or = FormulaFactory.eINSTANCE.createOr();
        final Not not_left = FormulaFactory.eINSTANCE.createNot();
        final Not not_right = FormulaFactory.eINSTANCE.createNot();
        not_left.setExp(this.<Formula>copy(((Nand)fm).getLeft()));
        not_right.setExp(this.<Formula>copy(((Nand)fm).getRight()));
        or.setLeft(this.<Not>copy(not_left));
        or.setRight(this.<Not>copy(not_right));
        return this.tseitin(this.<Or>copy(or));
      }
    }
    if (!_matched) {
      if (fm instanceof Or) {
        _matched=true;
        final Pair f1 = this.tseitin(this.<Formula>copy(((Or)fm).getLeft()));
        final Pair f2 = this.tseitin(this.<Formula>copy(((Or)fm).getRight()));
        final VarProp p = FormulaFactory.eINSTANCE.createVarProp();
        final Or or_1 = FormulaFactory.eINSTANCE.createOr();
        final Or or_2 = FormulaFactory.eINSTANCE.createOr();
        final Or or_3 = FormulaFactory.eINSTANCE.createOr();
        final Or or_4 = FormulaFactory.eINSTANCE.createOr();
        final And and_1 = FormulaFactory.eINSTANCE.createAnd();
        final And and_2 = FormulaFactory.eINSTANCE.createAnd();
        final And and_3 = FormulaFactory.eINSTANCE.createAnd();
        final And and_4 = FormulaFactory.eINSTANCE.createAnd();
        final Not not_p = FormulaFactory.eINSTANCE.createNot();
        final Not not_p1 = FormulaFactory.eINSTANCE.createNot();
        final Not not_p2 = FormulaFactory.eINSTANCE.createNot();
        p.setId(this.getRandomString());
        not_p.setExp(this.<VarProp>copy(p));
        not_p1.setExp(this.<Formula>copy(f1.p));
        not_p2.setExp(this.<Formula>copy(f2.p));
        or_1.setLeft(this.<Not>copy(not_p));
        or_2.setLeft(this.<Formula>copy(f1.p));
        or_2.setRight(this.<Formula>copy(f2.p));
        or_1.setRight(this.<Or>copy(or_2));
        or_3.setLeft(this.<VarProp>copy(p));
        or_3.setRight(this.<Not>copy(not_p1));
        or_4.setLeft(this.<VarProp>copy(p));
        or_4.setRight(this.<Not>copy(not_p2));
        and_1.setLeft(this.<Formula>copy(f1.c));
        and_1.setRight(this.<Formula>copy(f2.c));
        and_2.setLeft(this.<Or>copy(or_4));
        and_2.setRight(this.<And>copy(and_1));
        and_3.setLeft(this.<Or>copy(or_3));
        and_3.setRight(this.<And>copy(and_2));
        and_4.setLeft(this.<Or>copy(or_1));
        and_4.setRight(this.<And>copy(and_3));
        VarProp _copy = this.<VarProp>copy(p);
        And _copy_1 = this.<And>copy(and_4);
        return new Pair(_copy, _copy_1);
      }
    }
    if (!_matched) {
      if (fm instanceof Nor) {
        _matched=true;
        final And and = FormulaFactory.eINSTANCE.createAnd();
        final Not not_left = FormulaFactory.eINSTANCE.createNot();
        final Not not_right = FormulaFactory.eINSTANCE.createNot();
        not_left.setExp(this.<Formula>copy(((Nor)fm).getLeft()));
        not_right.setExp(this.<Formula>copy(((Nor)fm).getRight()));
        and.setLeft(this.<Not>copy(not_left));
        and.setRight(this.<Not>copy(not_right));
        return this.tseitin(this.<And>copy(and));
      }
    }
    if (!_matched) {
      if (fm instanceof Not) {
        _matched=true;
        final Pair f1 = this.tseitin(this.<Formula>copy(((Not)fm).getExp()));
        final Not not_p = FormulaFactory.eINSTANCE.createNot();
        not_p.setExp(this.<Formula>copy(f1.p));
        Not _copy = this.<Not>copy(not_p);
        Formula _copy_1 = this.<Formula>copy(f1.c);
        return new Pair(_copy, _copy_1);
      }
    }
    if (!_matched) {
      if (fm instanceof VarProp) {
        _matched=true;
        VarProp _copy = this.<VarProp>copy(((VarProp)fm));
        return new Pair(_copy, null);
      }
    }
    return _switchResult;
  }
  
  public Object toDimacs(final Formula fm, final LinkedHashMap<String, Integer> toInd, final Vector<String> clauses) {
    Object _switchResult = null;
    boolean _matched = false;
    if (fm instanceof Equiv) {
      _matched=true;
    }
    if (!_matched) {
      if (fm instanceof Implie) {
        _matched=true;
      }
    }
    if (!_matched) {
      if (fm instanceof Nand) {
        _matched=true;
      }
    }
    if (!_matched) {
      if (fm instanceof Nor) {
        _matched=true;
      }
    }
    if (_matched) {
      throw new RuntimeException("Not in CNF");
    }
    if (!_matched) {
      if (fm instanceof Or) {
        _matched=true;
        Object _dimacs = this.toDimacs(((Or)fm).getLeft(), toInd, clauses);
        String _plus = (_dimacs + " ");
        Object _dimacs_1 = this.toDimacs(((Or)fm).getRight(), toInd, clauses);
        return (_plus + _dimacs_1);
      }
    }
    if (!_matched) {
      if (fm instanceof And) {
        _matched=true;
        Formula _left = ((And)fm).getLeft();
        boolean _tripleNotEquals = (_left != null);
        if (_tripleNotEquals) {
          Formula _left_1 = ((And)fm).getLeft();
          if ((_left_1 instanceof And)) {
            this.toDimacs(((And)fm).getLeft(), toInd, clauses);
          } else {
            Object _dimacs = this.toDimacs(((And)fm).getLeft(), toInd, clauses);
            String _plus = (_dimacs + " 0\n");
            clauses.add(_plus);
          }
        }
        Formula _right = ((And)fm).getRight();
        boolean _tripleNotEquals_1 = (_right != null);
        if (_tripleNotEquals_1) {
          Formula _right_1 = ((And)fm).getRight();
          if ((_right_1 instanceof And)) {
            this.toDimacs(((And)fm).getRight(), toInd, clauses);
          } else {
            Object _dimacs_1 = this.toDimacs(((And)fm).getRight(), toInd, clauses);
            String _plus_1 = (_dimacs_1 + " 0\n");
            clauses.add(_plus_1);
          }
        }
        return "";
      }
    }
    if (!_matched) {
      if (fm instanceof Not) {
        _matched=true;
        Object _xifexpression = null;
        Formula _exp = ((Not)fm).getExp();
        if ((_exp instanceof Not)) {
          Object _xblockexpression = null;
          {
            Formula _copy = this.<Formula>copy(((Not)fm).getExp());
            final Not _not = ((Not) _copy);
            _xblockexpression = this.toDimacs(this.<Formula>copy(_not.getExp()), toInd, clauses);
          }
          _xifexpression = _xblockexpression;
        } else {
          Object _dimacs = this.toDimacs(((Not)fm).getExp(), toInd, clauses);
          _xifexpression = ("-" + _dimacs);
        }
        _switchResult = _xifexpression;
      }
    }
    if (!_matched) {
      if (fm instanceof VarProp) {
        _matched=true;
        Integer _elvis = null;
        Integer _get = null;
        if (toInd!=null) {
          _get=toInd.get(((VarProp)fm).getId());
        }
        if (_get != null) {
          _elvis = _get;
        } else {
          String _id = ((VarProp)fm).getId();
          int _size = toInd.size();
          int _plus = (_size + 1);
          Integer _put = toInd.put(_id, Integer.valueOf(_plus));
          _elvis = _put;
        }
        return toInd.get(((VarProp)fm).getId());
      }
    }
    return _switchResult;
  }
  
  public String printAST(final Formula fm) {
    String _switchResult = null;
    boolean _matched = false;
    if (fm instanceof PFormula) {
      _matched=true;
      String _printAST = this.printAST(((PFormula)fm).getExp());
      String _plus = ("(" + _printAST);
      _switchResult = (_plus + ")");
    }
    if (!_matched) {
      if (fm instanceof Equiv) {
        _matched=true;
        String _printAST = this.printAST(((Equiv)fm).getLeft());
        String _plus = ("Equiv(" + _printAST);
        String _plus_1 = (_plus + " , ");
        String _printAST_1 = this.printAST(((Equiv)fm).getRight());
        String _plus_2 = (_plus_1 + _printAST_1);
        _switchResult = (_plus_2 + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof Implie) {
        _matched=true;
        String _printAST = this.printAST(((Implie)fm).getLeft());
        String _plus = ("Implie (" + _printAST);
        String _plus_1 = (_plus + " , ");
        String _printAST_1 = this.printAST(((Implie)fm).getRight());
        String _plus_2 = (_plus_1 + _printAST_1);
        _switchResult = (_plus_2 + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof Nor) {
        _matched=true;
        String _printAST = this.printAST(((Nor)fm).getLeft());
        String _plus = ("Nor(" + _printAST);
        String _plus_1 = (_plus + " , ");
        String _printAST_1 = this.printAST(((Nor)fm).getRight());
        String _plus_2 = (_plus_1 + _printAST_1);
        _switchResult = (_plus_2 + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof Or) {
        _matched=true;
        String _printAST = this.printAST(((Or)fm).getLeft());
        String _plus = ("Or (" + _printAST);
        String _plus_1 = (_plus + " , ");
        String _printAST_1 = this.printAST(((Or)fm).getRight());
        String _plus_2 = (_plus_1 + _printAST_1);
        _switchResult = (_plus_2 + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof Nand) {
        _matched=true;
        String _printAST = this.printAST(((Nand)fm).getLeft());
        String _plus = ("Nand (" + _printAST);
        String _plus_1 = (_plus + " , ");
        String _printAST_1 = this.printAST(((Nand)fm).getRight());
        String _plus_2 = (_plus_1 + _printAST_1);
        _switchResult = (_plus_2 + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof And) {
        _matched=true;
        String _printAST = this.printAST(((And)fm).getLeft());
        String _plus = ("And (" + _printAST);
        String _plus_1 = (_plus + " , ");
        String _printAST_1 = this.printAST(((And)fm).getRight());
        String _plus_2 = (_plus_1 + _printAST_1);
        _switchResult = (_plus_2 + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof Not) {
        _matched=true;
        String _printAST = this.printAST(((Not)fm).getExp());
        String _plus = ("Not (" + _printAST);
        _switchResult = (_plus + ")");
      }
    }
    if (!_matched) {
      if (fm instanceof VarProp) {
        _matched=true;
        _switchResult = ((VarProp)fm).getId();
      }
    }
    return _switchResult;
  }
  
  public abstract Boolean test(final String file);
  
  public String testSatisfiability(final Formula formula) {
    String _xblockexpression = null;
    {
      final Pair f = this.tseitin(this.<Formula>copy(formula));
      final And and = FormulaFactory.eINSTANCE.createAnd();
      and.setLeft(this.<Formula>copy(f.c));
      and.setRight(this.<Formula>copy(f.p));
      final LinkedHashMap<String, Integer> toInd = CollectionLiterals.<String, Integer>newLinkedHashMap();
      final Vector<String> clauses = new Vector<String>();
      this.toDimacs(this.<And>copy(and), toInd, clauses);
      String res = "";
      for (final String str : clauses) {
        String _res = res;
        res = (_res + str);
      }
      String _format = String.format("p cnf %d %d\n", 
        Integer.valueOf(toInd.size()), Integer.valueOf(clauses.size()));
      final String dimacsStr = (_format + res);
      String _xifexpression = null;
      Boolean _test = this.test(dimacsStr);
      if ((_test).booleanValue()) {
        _xifexpression = "satifiable";
      } else {
        _xifexpression = "unsatifiable";
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
}
